package ru.megafon.testservices.integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.WebTarget;

/**
 * Этот сервис - один из многих подобных интеграционных сервисов. Они все делают запросы в биллинг, разными методами,
 * с разными параметрами.
 * Эти сервисы менять нельзя!
 */
@Service
public class IntegrationService {
    @Autowired
    private WebTarget target;

    public String readData() {
        String result = target.path("/").request().get(String.class);
        return result;
    }
}
