package ru.megafon.testservices;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

/**
 * Настройки различных интеграций, в нашем случае - одной
 */
@Configuration
public class IntegrationConfig {
    @Bean
    public WebTarget target() {
        // Настройка WebTarget-а в общем случае это сложная процедура, требующая, например, установки
        // кастомных фильтров, прокси-сервера, заголовков для авторизации, и т.п.
        // Здесь для простоты мы создаем бин без дополнительных параметров.
        return ClientBuilder.newBuilder().build().target("https://google.com");
    }
}
