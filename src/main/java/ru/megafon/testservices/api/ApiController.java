package ru.megafon.testservices.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.megafon.testservices.integration.IntegrationService;
import ru.megafon.testservices.zonesupport.RequestHolder;

import javax.ws.rs.core.MediaType;

@RestController
@RequestMapping("/api")
@Slf4j
public class ApiController {
    @Autowired
    private IntegrationService integrationService;

    @GetMapping(value = "/zone", produces = MediaType.TEXT_PLAIN)
    public String getZone() {
        log.info("isNewZone: " + RequestHolder.isNewZone());
        return Boolean.toString(RequestHolder.isNewZone());
    }

    @GetMapping(value = "/hello", produces = MediaType.TEXT_PLAIN)
    public String hello() {
        return integrationService.readData();
    }
}
