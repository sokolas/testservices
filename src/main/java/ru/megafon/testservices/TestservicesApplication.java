package ru.megafon.testservices;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
@Slf4j
public class TestservicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestservicesApplication.class, args);
        log.info("-------------------------");
        log.info("Отсутствие заголовка 'x-new-zone' в запросе означает, что пользователь из старой зоны");
        log.info("Любое непустое значение заголовка 'x-new-zone' означает, что пользователь из новой зоны");
        log.info("-------------------------");
    }

}
