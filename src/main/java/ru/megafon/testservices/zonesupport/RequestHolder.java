package ru.megafon.testservices.zonesupport;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;

@Component
@Slf4j
public class RequestHolder implements ServletRequestListener {
    private static ThreadLocal<String> newZone = new ThreadLocal<>();

    static void setNewZone(String newZone) {
        RequestHolder.newZone.set(newZone);
    };

    /**
     * Метод, позволяющий в любом месте кода в рамках обработки запроса узнать, из какой зоны пользователь
     * @return признак того, что пользователь из "новой" зоны
     */
    public static boolean isNewZone() {
        return newZone.get() != null;
    }

    static void reset() {
        newZone.remove();
    }


    /**
     * Сбрасываем контекст, когда обработка запроса завершилась
     */
    public void requestDestroyed(ServletRequestEvent event) {
        RequestHolder.reset();
    }

    /**
     * При инициализации реквеста установим зону абонента по заголовку x-new-zone
     */
    public void requestInitialized(ServletRequestEvent event) {
        HttpServletRequest request = (HttpServletRequest) event.getServletRequest();
        String header = request.getHeader("x-new-zone");
        log.info("x-new-zone: " + header);
        RequestHolder.setNewZone(header);
    }
}
