package ru.megafon.testservices;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.megafon.testservices.api.ApiController;
import ru.megafon.testservices.integration.IntegrationService;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestservicesApplicationTests {

    @Autowired
    private ApiController controller;

    @Autowired
    private IntegrationService service;

    @Test
    public void contextLoads() {
        assertThat(controller).isNotNull();
        assertThat(service).isNotNull();
    }

}
