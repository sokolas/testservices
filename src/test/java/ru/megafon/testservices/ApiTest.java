package ru.megafon.testservices;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void oldZoneUser() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/zone",
                String.class)).isEqualTo("false");
    }

    @Test
    public void newZoneUser() throws Exception {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("x-new-zone", "1");
        final HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "/api/zone",
                HttpMethod.GET, entity, String.class);
        assertThat(response.getBody()).isEqualTo("true");
    }
}
